# Efficient eigenvalue problems solution

Projekt ma na celu stworzenie prostego w obsłudze API mającego wspierać i automatyzować rozwiązywanie problemów własnych dla celów redukcji rzędu modelu.

## Autorzy
* Krzysztof Szenk
* Jakub Kuchta

Projekt został rozpoczęty w dniu 12.03.2019.
